#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWidget>
#include <QGridLayout>
#include <QPushButton>
#include <QTextEdit>
#include <QPieSeries>
#include <QChart>
#include <QChartView>
#include <QList>
#include <QFile>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void analyze();

private:
    Ui::MainWindow *ui;
    QTextEdit *textEdit;
    QPieSeries *pieChart;
};

#endif // MAINWINDOW_H
